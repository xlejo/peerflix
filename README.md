# Peerflix on Container

> Probado en Fedora Silverblue con Podman.

## Obteniendo imagen:

```bash
podman login -u GITLAB_USER registry.gitlab.com
podman pull registry.gitlab.com/xlejo/peerflix:latest
```

## Guardando en .zshrc (o .bashrc)
```bash
vim ~/.zshrc
```

Al principio de la linea poner lo siguiente:

```bash
alias peerflix="podman run --rm -p 8888:8888 -it registry.gitlab.com/xlejo/peerflix:latest peerflix"
```

Una vez que lo guardes, ejecutá: 

```bash
source ~/.zshrc
```

## Fuente:

[https://github.com/mafintosh/peerflix](https://github.com/mafintosh/peerflix)
